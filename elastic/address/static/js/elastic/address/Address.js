define([
    'dijit/_WidgetsInTemplateMixin',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetBase',
    'dojo/_base/declare',
    "./tastypie/Rest",
    "dojo/_base/Deferred",
    "dojo/cookie",
    'dojo/when',
    "country/Country",
    'dojo/text!./resources/address.html'
], function (WidgetsInTemplateMixin, TemplatedMixin, WidgetBase, declare, Rest, Deferred, cookie, when,
             Country, template) {
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        templateString: template,
        baseClass: 'address',
        recordId: undefined,
        store: undefined,
        _initRecord: null,
        _baseUrl: "/elastic/address/api/v1/address/",

        postMixInProperties: function () {
            this['inherited'](arguments);
        },

        buildRendering: function () {
            this['inherited'](arguments);
        },

        postCreate: function () {
            this['inherited'](arguments);
        },

        _buildRecord: function () {
            var record = {};
            record['country'] = this.get('country').get('value');
            record['city'] = this.get('city').get('value');
            record['district'] = this.get('district').get('value');
            record['street'] = this.get('street').get('value');
            record['house'] = this.get('house').get('value');
            record['flat'] = this.get('flat').get('value');
            record['postal_index'] = this.get('postalIndex').get('value');
            return record;
        },

        _isChanged: function () {
            if (!this._initRecord){ return false; }
            return JSON.stringify(this._initRecord) != JSON.stringify(this._buildRecord());
        },

        _load: function(url){
            var self = this;

            var store = Rest({target: url, headers: {"X-CSRFToken": cookie('csrftoken')}});

            return store.fetch().then(function(item) {
                self.recordId = item['id'];
                self.get('country').set('value',  item['country']['code']);
                self.get('city').set('value',  item['city']);
                self.get('district').set('value',  item['district']);
                self.get('street').set('value',  item['street']);
                self.get('house').set('value',  item['house']);
                self.get('flat').set('value',  item['flat']);
                self.get('postalIndex').set('value',  item['postal_index']);
            });
        },

        startup: function () {
            var self = this;
            this['inherited'](arguments);
            if (this.recordId){
                when(this._load(this._baseUrl + this.recordId + "/"), function(){
                    self._initRecord = self._buildRecord();
                });
            }
        },

        save: function(force /* Безусловное сохранение данных. По-умолчанию - отключено. */){

            force = force || false;

            var self = this, store;

            var d = new Deferred();

            if (!force && !this._isChanged()){
                d.resolve();
                return d;
            }

            var record = this._buildRecord();

            if (this.recordId){  // put exists record
                store = Rest({
                    target: this._baseUrl + this.recordId + "/",
                    headers: {"X-CSRFToken": cookie('csrftoken')}
                });
                record['id'] = this.recordId;
                store.put(record).then(
                    function(){
                        self._initRecord = self._buildRecord();
                        d.resolve();
                    },
                    function(e){ d.reject(e); }
                );
            }else{  // add new record
                store = Rest({target: this._baseUrl, headers: {"X-CSRFToken": cookie('csrftoken')}});
                store.on('add', function(event){
                    when(self._load(event.url), function(){
                        self._initRecord = self._buildRecord();
                        d.resolve();
                    });
                });
                store.add(record);
            }
            return d;
        }
    });
});
