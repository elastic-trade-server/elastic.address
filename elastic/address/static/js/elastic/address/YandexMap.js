define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/request/script"
], function(declare, _WidgetBase, _TemplatedMixin, script){


    return declare("elastic.address.Address", [_WidgetBase, _TemplatedMixin], {
        templateString: "<div style='width=auto; height=auto;'></div>",
        baseClass: "ymap",

        mapType: null,
        latitude: null,
        longitude: null,
        zoom: null,
        controls: ["default"],
        scrollZoom: false,

        balloonContentHeader: null,
        balloonContentBody: null,
        balloonContentFooter: null,
        hintContent: null,
        placeMarkStyle: null,

        _buildMap: function(){
            var self = this;


            this.latitude = typeof(this.latitude) == "string" ? Number(this.latitude.replace (/,/, '.')) : this.latitude;
            this.longitude = typeof(this.longitude) == "string" ? Number(this.longitude.replace (/,/, '.')) : this.longitude;
            this.zoom =  typeof(this.zoom) == "string" ? Number(this.zoom.replace (/,/, '.')) : this.zoom;

            var map = new window['ymaps'].Map(this.domNode.id, {
                center: [Number(this.latitude), Number(this.longitude)],
                zoom: Number(this.zoom),
                controls: this.controls
            });
            if (this.scrollZoom){
                map['behaviors'].enable('scrollZoom');
            }else{
                map['behaviors'].disable('scrollZoom');
            }
            var placeMark = new window['ymaps']['Placemark'](
                [self.latitude, self.longitude],
                {
                    balloonContentHeader: self.balloonContentHeader,
                    balloonContentBody: self.balloonContentBody,
                    balloonContentFooter: self.balloonContentFooter,
                    hintContent: self.hintContent
                },
                this.placeMarkStyle ? this.placeMarkStyle : {preset: 'islands#redIcon'}
            );
            map['geoObjects'].add(placeMark);
        },

        init: function(){
            var self = this;

            self._buildMap();
        },

        postCreate: function(){
            this['inherited'](arguments);
        },

        startup: function() {
            var self = this;

            script.get("http://api-maps.yandex.ru/2.1/?lang=ru_RU", {
                "jsonp": "onload"
            }).then(function(){
                self.init();
            }, function(err){
                console.error(err);
            });
        }
    });
});
