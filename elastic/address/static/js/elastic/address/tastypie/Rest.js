define([
	'dojo/request',
	'dojo/when',
	'dojo/_base/lang',
	'dojo/_base/declare',
	'../dstore/Rest'
], function (request, when, lang, declare, Rest /*=====, Store =====*/) {

	return declare(Rest, {

		put: function (object, options) {
			// summary:
			//		Stores an object. This will trigger a PUT request to the server
			//		if the object has an id, otherwise it will trigger a POST request.
			// object: Object
			//		The object to store.
			// options: __PutDirectives?
			//		Additional metadata for storing the data.  Includes an 'id'
			//		property if a specific id is to be used.
			// returns: dojo/_base/Deferred
			options = options || {};
			var id = ('id' in options) ? options.id : this.getIdentity(object);
			var hasId = typeof id !== 'undefined';
			var store = this;

			var positionHeaders = 'beforeId' in options
				? (options.beforeId === null
					? { 'Put-Default-Position': 'end' }
					: { 'Put-Before': options.beforeId })
				: (!hasId || options.overwrite === false
					? { 'Put-Default-Position': (this.defaultNewToStart ? 'start' : 'end') }
					: null);

			var initialResponse = request(hasId ? this._getTarget(id) : this.target, {
				method: hasId && !options.incremental ? 'PUT' : 'POST',
				data: this.stringify(object),
				headers: lang.mixin({
					'Content-Type': 'application/json',
					Accept: this.accepts,
					'If-Match': options.overwrite === true ? '*' : null,
					'If-None-Match': options.overwrite === false ? '*' : null
				}, positionHeaders, this.headers, options.headers)
			});
			initialResponse.then(function (response) {
				var event = {};

				if ('beforeId' in options) {
					event.beforeId = options.beforeId;
				}

				event.target = response && store._restore(store.parse(response), true) || object;

				when(initialResponse.response, function (httpResponse) {
					event.url = httpResponse.xhr.getResponseHeader("Location");
					event.response = httpResponse;
					store.emit(httpResponse.status === 201 ? 'add' : 'update', event);
				});

			});
			return initialResponse;
		},

		_renderFilterParams: function (filter) {
			// summary:
			//		Constructs filter-related params to be inserted into the query string
			// returns: String
			//		Filter-related params to be inserted in the query string
            if (filter.args[1] == "*"){ return []; }
			return [encodeURIComponent(filter.args[0]) + '=' + encodeURIComponent(filter.args[1])];
		}

	});

});
