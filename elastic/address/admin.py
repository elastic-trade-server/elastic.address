# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *
from forms import AddressForm

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Address package"

class YandexMapInline(admin.StackedInline):
    model = YandexMap


class AddressAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    form = AddressForm

    inlines = [
        YandexMapInline,
    ]

admin.site.register(Address, AddressAdmin)
