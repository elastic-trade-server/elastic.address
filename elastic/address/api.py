# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from django.utils.translation import ugettext_lazy as _
from tastypie.authorization import Authorization

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Address package"


class YandexMapResource(ModelResource):
    class Meta:
        queryset = YandexMap.objects.all()
        resource_name = 'yandex_map'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False

    map_type = fields.CharField(attribute='map_type', help_text=_('Map type'))
    latitude = fields.CharField(attribute='latitude', help_text=_('Latitude'))
    longitude = fields.CharField(attribute='longitude', help_text=_('Longitude'))
    zoom = fields.IntegerField(attribute='zoom', help_text=_('Zoom'))
    hint = fields.CharField(attribute='hint', help_text=_('Hint'))
    balloon_header = fields.CharField(attribute='balloon_header', null=True, blank=True, help_text=_('Balloon header'))
    balloon_body = fields.CharField(attribute='balloon_body', null=True, blank=True, help_text=_('Balloon body'))
    balloon_footer = fields.CharField(attribute='balloon_footer', null=True, blank=True, help_text=_('Balloon footer'))
    address = fields.ToOneField("elastic.address.api.AddressResource", attribute='address', full=False,
                                help_text=_('Address'))

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        bundle.data.pop('address')
        return bundle


class AddressResource(ModelResource):
    class Meta:
        queryset = Address.objects.all()
        resource_name = 'address'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        filtering = {
            "country": ALL,
            "city": ALL,
            "postal_index": ALL,
        }

    country = fields.ToOneField("elastic.country.api.CountryResource", attribute='country', full=True,
                                help_text=_('Country'))
    city = fields.CharField(attribute='city', help_text=_('City'))
    district = fields.CharField(attribute='district', null=True, blank=True, help_text=_('District'))
    street = fields.CharField(attribute='street', help_text=_('Street'))
    house = fields.CharField(attribute='house', help_text=_('House'))
    flat = fields.CharField(attribute='flat', null=True, blank=True, help_text=_('Flat'))
    postal_index = fields.CharField(attribute='postal_index', help_text=_('Postal Index'))
    address_string = fields.CharField(help_text=_('Address string'))
    ymaps = fields.ToManyField(YandexMapResource, attribute='ymaps', null=True, blank=True, full=True,
                               readonly=True, help_text=_('Yandex maps'))

    @staticmethod
    def hydrate_country(bundle):
        if 'country' not in bundle.data:  # Если страна не указана, то выставляем "Россия"
            bundle.obj.country = Country.objects.get(code=643)
            return bundle

        # Если указан код страны, то ищем запись по справочнику
        if type(bundle.data['country']) in (unicode, str, int):
            bundle.obj.country = Country.objects.get(code=bundle.data['country'])
            bundle.data.pop('country')
        return bundle

    @staticmethod
    def dehydrate_address_string(bundle):
        return unicode(bundle.obj)

    @staticmethod
    def dehydrate_country(bundle):
        return str(bundle.obj.country.code)

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle
