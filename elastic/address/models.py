# -*- coding: utf-8 -*-


from django.db import models
from django.utils.translation import ugettext_lazy as _
from elastic.country.models import Country
import urllib2
import json

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Address package"


class Address(models.Model):
    """
    Адрес
    """
    class Meta:
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')
        # unique_together = ("country", "city", "street", "house", "flat", "postal_index", )

    #: Указатель на элемент справочника "Страны"
    country = models.ForeignKey(Country, verbose_name=_('Country'))

    #: Населенный пункт
    city = models.CharField(verbose_name=_('City'), max_length=128)

    #: Район
    district = models.CharField(verbose_name=_('District'), max_length=128, null=True, blank=True)

    #: Улица
    street = models.CharField(verbose_name=_('Street'), max_length=256)

    #: Строение
    house = models.CharField(verbose_name=_('House'), max_length=56)

    #: Апартаменты
    flat = models.CharField(verbose_name=_('Flat'), null=True, blank=True, max_length=56)

    #: Почтовый индекс
    postal_index = models.CharField(verbose_name=_('Postal Index'), max_length=16)

    def __unicode__(self):
        city = self.city if not self.district else u"{0}, {1}".format(self.city, self.district)
        return u"{0} - {1}, {2}, {3} {4}, {5}".format(
            self.country, self.postal_index, city, self.street, self.house, self.flat,
            ) if self.flat else u"{0} - {1}, {2}, {3} {4}".format(
            self.country, self.postal_index, city, self.street, self.house)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Address, self).save(force_insert, force_update, using, update_fields)
        if not self.ymaps.count():
            geo_url = "http://geocode-maps.yandex.ru/1.x/?geocode=" + \
                      "+".join(self.__unicode__().split(" ")) + "&format=json"
            try:
                request = urllib2.Request(
                    geo_url.encode("UTF-8"),
                    None,
                    {
                        "Cache-Control": "no-cache",
                        "Content-Type": "application/json",
                    }
                )
                data = json.loads(urllib2.urlopen(request).read())['response']

                if len(data['GeoObjectCollection']):
                    self.ymaps.create(
                        hint=data['GeoObjectCollection']['featureMember'][0]['GeoObject']['name'],
                        longitude=data[
                            'GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'].split(' ')[0],
                        latitude=data[
                            'GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'].split(' ')[1]
                    )
                print data
            except urllib2.HTTPError:
                pass


class YandexMap(models.Model):
    """
    Карта проезда (Yandex)
    """

    MAP_TYPE = (
        ('yandex#map', _('Map')),  # Схема
        ('yandex#satellite', _('Satellite')),  # Спутник
        ('yandex#hybrid', _('Hybrid')),  # Гибрид
        ('yandex#publicMap', _('Public map')),  # Народная карта
        ('yandex#publicMapHybrid', _('Public map hybrid')),  # Народный гибрид
    )

    #: Тип карты
    map_type = models.CharField(max_length=16, choices=MAP_TYPE, default='yandex#map', verbose_name=_('Map type'))

    #: Герграфическая широта
    latitude = models.DecimalField(max_digits=10, decimal_places=6, verbose_name=_('Latitude'))

    #: Герграфическая долгота
    longitude = models.DecimalField(max_digits=10, decimal_places=6, verbose_name=_('Longitude'))

    #: Масштаб
    zoom = models.PositiveIntegerField(default=15, verbose_name=_('Zoom'))

    #: Всплывающая подсказка
    hint = models.CharField(max_length=256, verbose_name=_('Hint'))

    #: Заголовок метки
    balloon_header = models.CharField(max_length=256, null=True,  blank=True, verbose_name=_('Balloon header'))

    #: Тело метки
    balloon_body = models.CharField(max_length=512, null=True,  blank=True, verbose_name=_('Balloon body'))

    #: Подвал метки
    balloon_footer = models.CharField(max_length=256, null=True,  blank=True, verbose_name=_('Balloon footer'))

    #: Указатель на элемент справочника "Адреса"
    address = models.ForeignKey(Address, related_name='ymaps', verbose_name=_('Address'))
