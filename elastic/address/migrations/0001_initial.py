# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('country', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(max_length=128, verbose_name='City')),
                ('district', models.CharField(max_length=128, null=True, verbose_name='District', blank=True)),
                ('street', models.CharField(max_length=256, verbose_name='Street')),
                ('house', models.CharField(max_length=56, verbose_name='House')),
                ('flat', models.CharField(max_length=56, null=True, verbose_name='Flat', blank=True)),
                ('postal_index', models.CharField(max_length=16, verbose_name='Postal Index')),
                ('country', models.ForeignKey(verbose_name='Country', to='country.Country')),
            ],
            options={
                'verbose_name': 'Address',
                'verbose_name_plural': 'Addresses',
            },
        ),
        migrations.CreateModel(
            name='YandexMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('map_type', models.CharField(default=b'yandex#map', max_length=16, verbose_name='Map type', choices=[(b'yandex#map', 'Map'), (b'yandex#satellite', 'Satellite'), (b'yandex#hybrid', 'Hybrid'), (b'yandex#publicMap', 'Public map'), (b'yandex#publicMapHybrid', 'Public map hybrid')])),
                ('latitude', models.DecimalField(verbose_name='Latitude', max_digits=10, decimal_places=6)),
                ('longitude', models.DecimalField(verbose_name='Longitude', max_digits=10, decimal_places=6)),
                ('zoom', models.PositiveIntegerField(default=15, verbose_name='Zoom')),
                ('hint', models.CharField(max_length=256, verbose_name='Hint')),
                ('balloon_header', models.CharField(max_length=256, null=True, verbose_name='Balloon header', blank=True)),
                ('balloon_body', models.CharField(max_length=512, null=True, verbose_name='Balloon body', blank=True)),
                ('balloon_footer', models.CharField(max_length=256, null=True, verbose_name='Balloon footer', blank=True)),
                ('address', models.ForeignKey(related_name='ymaps', verbose_name='Address', to='address.Address')),
            ],
        ),
    ]
