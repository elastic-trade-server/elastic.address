from django.conf.urls import patterns, include, url
from tastypie.api import Api
from api import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Address package"

v1_api = Api(api_name='v1')
v1_api.register(AddressResource())
v1_api.register(YandexMapResource())

urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),
)
